function firstInitialization(){
	stats = new Stats();
	container.appendChild( stats.dom );
	camera = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 0.1, 100 );
	camera.position.x = camera_x;
	camera.position.z = camera_z;
	camera.position.y = camera_y;
	camera.lookAt(new THREE.Vector3(lookAtX, lookAtY, lookAtZ));
	scene = new THREE.Scene();
	//add the fog
	scene.fog = new THREE.Fog(0xaaaaaa, 0.2, 50);
	renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
	renderer.physicallyCorrectLights = true;
	renderer.gammaInput = true;
	renderer.gammaOutput = true;
	renderer.shadowMap.enabled = true;
	renderer.toneMapping = THREE.ReinhardToneMapping;
	renderer.setPixelRatio( window.devicePixelRatio/2 );
	renderer.setSize( window.innerWidth, window.innerHeight );
	container.appendChild( renderer.domElement );
	window.addEventListener( 'resize', onWindowResize, false );
}
//create room
function createRoom(z){
	floorMat = floorMat0;
	wallMat = wallMat0;
	ceilingMat = ceilingMat0;
	if(selected_texture == 1){
		floorMat = floorMat1;
		wallMat = wallMat1;
		ceilingMat = ceilingMat1;		
	}
	
	//floor
	var floorMesh = new THREE.Mesh( floorGeometry, floorMat );
	floorMesh.receiveShadow = true;
	floorMesh.rotation.x = -Math.PI / 2.0;
	floorMesh.position.y = floor_position_x;
	floorMesh.position.z = z;
	//left
	var leftWallMesh = new THREE.Mesh( wallGeometry, wallMat );
	leftWallMesh.receiveShadow = true;
	leftWallMesh.rotation.y = Math.PI / 2.0;
	leftWallMesh.position.x = -floor_width/2;
	leftWallMesh.position.z = wall_width/2;
	floorMesh.add(leftWallMesh);
	//right
	var rightWallMesh = new THREE.Mesh( wallGeometry, wallMat );
	rightWallMesh.receiveShadow = true;
	rightWallMesh.rotation.y = -Math.PI / 2.0;
	rightWallMesh.position.x = floor_width/2;
	rightWallMesh.position.z = wall_width/2;
	floorMesh.add(rightWallMesh);
	//ceiling
	var ceilingMesh = new THREE.Mesh( ceilingGeometry, ceilingMat );
	ceilingMesh.receiveShadow = true;
	ceilingMesh.rotation.y = -Math.PI;
	ceilingMesh.position.x = 0;
	ceilingMesh.position.z = wall_width;
	floorMesh.add(ceilingMesh);
	if(!first){					
		var laser = generateRandomLaser(-room_length/2);
		floorMesh.add( laser );
		var laser2 = generateRandomLaser(-room_length/4);
		floorMesh.add( laser2 );
		var laser3 = generateRandomLaser(0);
		floorMesh.add( laser3 );
		var laser4 = generateRandomLaser(room_length/4);
		floorMesh.add( laser4 );
		laserList.push(laser);
		laserList.push(laser2);
		laserList.push(laser3);
		laserList.push(laser4);
	}
	return floorMesh;
}
function generateRandomLaser(y){
	var laser = new THREE.PointLight( 0xFF0000, laser_intensity, 20, 2 );
	laser.add( new THREE.Mesh( laserGeometry, laserMat ) );
	laser.castShadow = true;
	// take random type from array
	randomType = laserType[Math.floor(Math.random() * laserType.length)];
	switch(randomType){
		case 0:
			laser.rotation.z = -Math.PI/2.0;
			laser.position.y = y;
			laser.position.z = zType0[Math.floor(Math.random() * zType0.length)];
			break;
		case 1:
			laser.rotation.x = -Math.PI/2.0;
			laser.position.x = xType1[Math.floor(Math.random() * xType1.length)];
			laser.position.y = y;
			break;
		case 2:
			laser.rotation.set(0, -0.68, -Math.PI/2); 
			laser.setRotationFromEuler(laser.rotation);
			laser.position.z = zType23[Math.floor(Math.random() * zType23.length)];
			laser.position.y = y;
			break;
		case 3:
			laser.rotation.set(0, 0.68, -Math.PI/2); 
			laser.setRotationFromEuler(laser.rotation);
			laser.position.z = zType23[Math.floor(Math.random() * zType23.length)];
			laser.position.y = y;
			break;
	}
	return laser;
}
//crash bb8
function crashed()
{
	if(lives_counter < 3)
	{
		leftArm.position.y = 7000;
	}
	if(lives_counter < 2)
	{
		rightArm.position.y = 7000;
	}
}
function hitByLaser()
{

	
	if(leftArm.position.z < 4 && lives_counter == 3)
	{
		leftArm.position.y += 0.5;
		leftArm.position.z += 0.5;
		leftArm.rotation.x += 0.3;
		leftArm.rotation.z += 1;
	}
	else if(rightArm.position.z < 4 && lives_counter == 2)
	{
		rightArm.position.y += 0.5;
		rightArm.position.z += 0.5;
		rightArm.rotation.x += 0.3;
		rightArm.rotation.z += 1;
	}
	
	else if(group.position.x < bb_limit_x && lives_counter < 2)
	{
		group.position.x += 0.2;
		body.rotation.x -= 0.1;
		head.position.y += 10;
		head.position.z += 15;
		head.rotation.x += 0.3;
		head.rotation.z += 1;
		room1.position.z += world_velocity;
		room2.position.z += world_velocity;
	}
	else
	{
		paused = true;
		score_value.innerHTML = score;
		myscore.style.display = "block";
		reset();
	}				
}

function reset(){

	
	lives_counter -= 1;

	if(lives_counter ==2 ){
		battery.src="img/half_battery.png";
	}
	else if(lives_counter == 1){
		battery.src="img/low_battery.png";
	}
	else{
		battery.src="img/full_battery.png";
	}

	if(lives_counter == 0){
		document.getElementById("score_value2").innerHTML =score;
		blackScreen.style.display="block";
		score = 0;
		lives_counter = lives;
	}

	laserList = [];
	room1.geometry.dispose();
	room1.material.dispose();
	scene.remove(room1);
	first = true;
	room1 = createRoom(-(room_length/2-camera_z));
	scene.add( room1 );
	first = false;

	room2.geometry.dispose();
	room2.material.dispose();
	scene.remove(room2);
	room2 = createRoom(room1.position.z-(room_length));
	scene.add(room2);

	head.position.x = head.position.y = head.position.z = head.rotation.x = head.rotation.y = head.rotation.z = 0;
	group.position.y = bb8_base;
	group.position.z = group.position.x = 0;
	leftArm.position.x = group.position.x - 0.18;
    leftArm.position.y = group.position.y + 0.5;
    leftArm.position.z = group.position.z + 0.2;
    leftArm.rotation.x = Math.PI/2;
    leftArm.rotation.z = -Math.PI/2;
    rightArm.position.x = group.position.x + 0.2;
    rightArm.position.y = group.position.y +0.5;
    rightArm.position.z = group.position.z + 0.2;
    rightArm.rotation.x = Math.PI/2;
    rightArm.rotation.z = -Math.PI/2;
    copyPos(group,copyGroup);
	paused = true;
	hitLaser = false;

	audio.pause();
	audio.currentTime = 0;
	audio.play();

	camera.position.x = camera_x;
	camera.position.z = camera_z;
	world_velocity = firstVelocity;
}

function copyPos(mesh1,mesh2){
	mesh1.position.x = mesh2.position.x;
	mesh1.position.y = mesh2.position.y;
	mesh1.position.z = mesh2.position.z;
}

function removeAllChild(actualMesh){
	if(actualMesh.children.length > 0){
		removeAllChild(actualMesh.children[actualMesh.children.length-1]);
		removeAllChild(actualMesh);
	}
	else{
		if(actualMesh.parent != null){
			//if(actualMesh.type == "Mesh"){
				if ( actualMesh.material != null){
					actualMesh.material.dispose();
				}
				if ( actualMesh.geometry != null){
					actualMesh.geometry.dispose();
				}
				if ( actualMesh.texture != null){
					actualMesh.texture.dispose();
				} 
			
			actualMesh.parent.remove(actualMesh);
		}
		else{
			actualMesh = null;
		}
	}
}
function backToMenu(){

	removeAllChild(scene);
	score = 0;
	lives_counter = lives;
	
	doRender = false;
	cover.style.display ="none";
	document.getElementById( 'menu' ).style.display="block";
	myscore.style.display = "none";
	blackScreen.style.display="none";
	document.getElementById("container-bar").style.display="block";
	score_value.innerHTML = score;
	container.style.display="none";
	battery_container.style.display="none";
	audio.pause();
	audio.currentTime = 0;
	
	battery.src="img/full_battery.png";
	laserList = [];
	room1.geometry.dispose();
	room1.material.dispose();
	scene.remove(room1);
	first = true;
	room1 = createRoom(-(room_length/2-camera_z));
	scene.add( room1 );
	first = false;

	room2.geometry.dispose();
	room2.material.dispose();
	scene.remove(room2);
	room2 = createRoom(room1.position.z-(room_length));
	scene.add(room2);

	head.position.x = head.position.y = head.position.z = head.rotation.x = head.rotation.y = head.rotation.z = 0;
	bb.position.y = bb8_base;
	bb.position.z = bb.position.x = 0;
	leftArm.position.x = bb.position.x - 0.18;
    leftArm.position.y = bb.position.y + 0.5;
    leftArm.position.z = bb.position.z + 0.2;
    leftArm.rotation.x = Math.PI/2;
    leftArm.rotation.z = -Math.PI/2;
    rightArm.position.x = bb.position.x + 0.2;
    rightArm.position.y = bb.position.y +0.5;
    rightArm.position.z = bb.position.z + 0.2;
    rightArm.rotation.x = Math.PI/2;
    rightArm.rotation.z = -Math.PI/2;

	copyPos(group,copyGroup);
	
	paused = true;
	hitLaser = false;

	camera.position.x = camera_x;
	camera.position.z = camera_z;
	world_velocity = firstVelocity;
}
function collisionBB(){
	for(i = 0;i<laserList.length;i++){
		var bbShape = new THREE.Box3().setFromObject(bb);
		if(laserList[i].parent==null)
			continue;
		laserList[i].parent.updateMatrixWorld();
		var vector = new THREE.Vector3();
		vector.setFromMatrixPosition( laserList[i].matrixWorld );
		laserList[i].updateMatrixWorld();
		var laserShape = new THREE.Box3().setFromObject(laserList[i]);
		if(vector.z > bb.position.z)
					laserList.shift();
		if(bbShape.isIntersectionBox(laserShape)){
			return true;
		}
	}
	return false;
}
