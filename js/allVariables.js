// Variable for css
var cover = document.getElementById("cover");
var loaded_var =  document.getElementById("loaded");
var audio = document.createElement('audio');
var source = document.createElement('source');
var score_value = document.getElementById("score_value");
var myscore = document.getElementById("myscore");
var battery = document.getElementById("battery");
var battery_container = document.getElementById("battery_container");
var blackscreen = document.getElementById("blackScreen");
var gameover = document.getElementById("gameover");

// where we add acene
var container = document.getElementById( 'container' );
//bar-progress
var elem = document.getElementById("myBar"); 
//check webgl
if ( ! Detector.webgl ) Detector.addGetWebGLMessage();
// Variables for parametrization
var room_length = 50;
var floor_width = 5;
var wall_width = 4;
var bb_limit_y = wall_width/2 + 0.6;
var bb_limit_x = floor_width/2 - 0.1*floor_width;
var floor_position_x = -1;
var camera_z = 3.5;
var camera_x = 0;
var camera_y = 1.5;
var laser_intensity = 150;
var firstVelocity = 0.15;
var gap_max = 1.5;
var velocity_camera = 0.3;
var camera_limit_y = 2.5;
var camera_limit_z = 4.5;
var camera_limit_x = 1.5;
var lives = 3; 
var lives_counter = lives;
var doRender = false;
var world_velocity = firstVelocity;
var increasing_velocity = 0.0025;
// var for parametrize bb8 y_base from floor
var bb8_base = -0.7;
var group_base = 0;
// camera lookAt Variable
var lookAtX = 0;
var lookAtY = 0;
var lookAtZ = -1.5;
var lookAtPositionFirst = [lookAtX,lookAtY,lookAtZ];
//Select BB model ( texture )
var bb_model = 0; // 0 orange 1 blue 2 green
var bb_models_path = ["models/bb-unit-threejs/bb-unit.js","models/bb-unit-threejs/bb-unit-blue.js","models/bb-unit-threejs/bb-unit-green.js"];
// Variable for checking if is first time
// for creating room1 and room2 the first time
var first = true;
//global score variable
var score = 0;
//pause var
var paused = true;
//inizialize the keyboard listner
var keyboard = new THREEx.KeyboardState();
// variable for texture
var env_texture = 0;	
	var texture0 = {
		floor:"img/texture_spacecraft/texture_floor.jpg",
		wall:"img/texture_spacecraft/texture_wall.jpg",
		ceiling:"img/texture_spacecraft/texture_ceiling.jpg"
	}
	var texture1 = {
		floor:"img/texture_general/texture_floor.jpg",
		wall:"img/texture_general/texture_wall.jpg",
		ceiling:"img/texture_general/texture_ceiling.jpg"
	}


	// Initialize all variable and set to Null
var camera, scene, renderer,
bulbLight, bulbMat, ambientLight,
object, loader, stats, bulbLight2,bulbLight3,bulbLight4, ceilingMesh,ceilingGeometry,rightWallGeometry ,rightWallMesh,rightWallMat,ceilingMat,
	floorMesh,floorGeometry,leftWallGeometry,leftWallMat,floorMat,room1,room2,randomType,app;
//our bb8
var bb,head,body,antenna,firstBody,firstHead,firstBB,leftArm,rightArm; 
bb = new THREE.Mesh();
head = new THREE.Mesh();
body = new THREE.Mesh();
antenna = new THREE.Mesh();
firstBody = new THREE.Mesh();
firstHead = new THREE.Mesh();
firstBB = new THREE.Mesh();
leftArm = new THREE.Mesh();
rightArm = new THREE.Mesh();


// bb's arms clour
var colorArms;
//new supergroup containing all BB-8 meshes (body + arms)
var group = new THREE.Group();
var copyGroup = new THREE.Group();
var loader = new THREE.ObjectLoader();
// Light Variable
var lightOn = 1; // set from menu if light is on or not
var bulbGeometry = new THREE.SphereGeometry( 0.02, 16, 8 );
var bulbMat = new THREE.MeshStandardMaterial( {
	emissive: 0xffffee,
	emissiveIntensity: 100,
	color: 0x000000
});
var bulbLuminousPowers = 1500;
var params = {
	shadows: true,
	exposure: 0.8,
	bulbPower: bulbLuminousPowers,
};
// LASER Variables
var laserMat = new THREE.MeshStandardMaterial( {
	emissive: 0xFF0000,
	emissiveIntensity: 100,
	color: 0x000000,
	blending: THREE.AdditiveBlending
});
var laserGeometry = new THREE.CylinderGeometry( 0.025, 0.025, 8, 32 );
// Laser type 
// 0 horizontal - 1 vertical - 2 oblique/right - 3 oblique \  left
var laserType = [ 0, 1];
var zType0 = [0.3,0.5,1.0,3.0,4.0,5.0,6.5,7.0].map(function(x) { return x * 0.5; });
var xType1 = [-3.0,-1.5,0.0,1.5,3.0].map(function(x) { return x * 0.5; });
var laserList = [];

// Room Texture and Materials
var selected_texture = 0; //default loaded texture 0
var floorGeometry = new THREE.PlaneBufferGeometry( floor_width, room_length );
var floor_texture0 = new THREE.TextureLoader().load(texture0.floor);
floor_texture0.repeat.set( 2, 10 );
floor_texture0.wrapS = THREE.RepeatWrapping;
floor_texture0.wrapT = THREE.RepeatWrapping;
floor_texture0.anisotropy = 4;
var floorMat0 = new THREE.MeshPhongMaterial( { map: floor_texture0 } );
floorMat0.needsUpdate = true;

var wallGeometry = new THREE.PlaneBufferGeometry( floor_width, room_length );
var wall_texture0 = new THREE.TextureLoader().load(texture0.wall);
wall_texture0.repeat.set( 2, 10 );
wall_texture0.wrapS = THREE.RepeatWrapping;
wall_texture0.wrapT = THREE.RepeatWrapping;
wall_texture0.anisotropy = 4;
var wallMat0 = new THREE.MeshPhongMaterial( { map: wall_texture0 } );
wallMat0.needsUpdate = true;

var ceilingGeometry = new THREE.PlaneBufferGeometry( floor_width, room_length );
var ceiling_texture0 = new THREE.TextureLoader().load(texture0.ceiling);
ceiling_texture0.repeat.set( 2, 10 );
ceiling_texture0.wrapS = THREE.RepeatWrapping;
ceiling_texture0.wrapT = THREE.RepeatWrapping;
ceiling_texture0.anisotropy = 4;
var ceilingMat0 = new THREE.MeshPhongMaterial( { map: ceiling_texture0 } );
ceilingMat0.needsUpdate = true;

// texture1
var floor_texture1 = new THREE.TextureLoader().load(texture1.floor);
floor_texture1.repeat.set( 2, 10 );
floor_texture1.wrapS = THREE.RepeatWrapping;
floor_texture1.wrapT = THREE.RepeatWrapping;
floor_texture1.anisotropy = 4;
var floorMat1 = new THREE.MeshPhongMaterial( { map: floor_texture1 } );
floorMat1.needsUpdate = true;

var wall_texture1 = new THREE.TextureLoader().load(texture1.wall);
wall_texture1.repeat.set( 2, 10 );
wall_texture1.wrapS = THREE.RepeatWrapping;
wall_texture1.wrapT = THREE.RepeatWrapping;
wall_texture1.anisotropy = 4;
var wallMat1 = new THREE.MeshPhongMaterial( { map: wall_texture1 } );
wallMat1.needsUpdate = true;

var ceiling_texture1 = new THREE.TextureLoader().load(texture1.ceiling);
ceiling_texture1.repeat.set( 2, 10 );
ceiling_texture1.wrapS = THREE.RepeatWrapping;
ceiling_texture1.wrapT = THREE.RepeatWrapping;
ceiling_texture1.anisotropy = 4;
var ceilingMat1 = new THREE.MeshPhongMaterial( { map: ceiling_texture1 } );
ceilingMat1.needsUpdate = true;